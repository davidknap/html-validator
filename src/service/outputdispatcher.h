#ifndef OUTPUTDISPATCHER_H
#define	OUTPUTDISPATCHER_H

#include <algorithm>
#include <string>
#include <iostream>
#include <fstream>
#include <map>
#include "formatter.h"
#include "../model/element.h"
#include "../exceptions/filedispatcherexception.h"
using namespace std;


/**
	* Output file handler
	*/
class OutputDispatcher {
protected:
		/** Output file name */
		string filename;
		/** File handler for output file */
		ofstream output;
		/** Current line number */
		int line;
		/** Current column */
		int column;

		/**
			* Low-level order to flush string to file.
   * @param out String to flush.
   */
		void writeToFile(const string& out);
public:
  /**
   * Create new output file.
   * @param filename File name
   * @throws FileDispatcherException File already exist.
   */
  OutputDispatcher(const string & filename);
		
		/**
			* Destructor.
   */
		~OutputDispatcher();
		
		/**
			* Format tag and print it to output.
			* @param e Pointer to filled structure with tag
   */
  void write(Element * e);
		
		/**
			* Retrieve current line number.
			* @return Output file cursor line number.
			*/
		int getLineNo();
		
		/**
			* Retrieve current column number.
			* @return Output file cursor column number.
			*/
		int getColumnNo();
		
		/**
			* Retrieve output file name.
   * @return Output file name.
   */
		string getFileName();
};


#endif	/* OUTPUTDISPATCHER_H */

