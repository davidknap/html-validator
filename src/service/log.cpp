#include "log.h"


string Log::last_msg = "";
ostream* Log::stream = &cout;
int Log::verbosity = 0;
bool Log::colorized = false;

void Log::Verbosity(int level) {
  if (level < Log::NONE || level > Log::DEBUG)
    throw LogException("Unsupported verbosity level");
  verbosity = level;
}

void Log::Output(ostream& str) {
  stream = &str;
}

void Log::Color(bool c) {
  colorized = c;
}

void Log::Error(const string & m) {
  print(string("[ERR] ") += m, Log::ERRORS, "\033[1;31m");
}

void Log::Error(Exception * e) {
  Error(e->getMessage());
}

void Log::Warning(const string & m) {
  print(string("[WRN] ") += m, Log::WARNINGS, "\033[1;33m");
}

void Log::Info(const string & m) {
  print(string("[INF] ") += m, Log::INFO, "\033[1;37m");
}

void Log::Debug(const string & m) {
  print(string("[DBG] ") += m, Log::DEBUG, "\033[37m");
}

string Log::last() {
  return last_msg;
}

void Log::print(string msg, int level, string color) {
  if (!stream)
    throw LogException("Output stream failed.");
		if (colorized) {
				msg = color += msg;
				msg += string("\033[0m");
		}
  if (level <= verbosity)
    *stream << msg << endl;
  last_msg = msg;
}
