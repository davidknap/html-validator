#ifndef LOG_H
#define	LOG_H

#include <iostream>
#include <string>
#include "../exceptions/logexception.h"
using namespace std;

/**
	* Logging service.
	* Customizable logging service that can be simply injected to any place in project.
	*/
class Log {
  static string last_msg;
		static ostream* stream;
		static int verbosity;
		static bool colorized;
		
		static void print(string msg, int level, string color);

public:
  /** Log level: Do not show anything. */
  static const int NONE = 0;
		/** Log level: Show only fatal errors. */
  static const int ERRORS = 1;
		/** Log level: Show warnings and fatal errors. */
  static const int WARNINGS = 2;
		/** Log level: Show more informations about results. */
  static const int INFO = 3;
		/** Log level: Show debug informations about implementation. */
  static const int DEBUG = 4;

		/**
			* Sets verbosity of log output. All lesser messages will be droped.
   * @param level Level of verbosity. Possible values: Log::NONE, Log::ERRORS, Log::WARNINGS, Log::INFO, Log::DEBUG.
			* @throw LogException Unknown verbosity level.
   */
  static void Verbosity(int level);
		
		/**
			* Sets output stream of log.
   * @param str Output stream to flush log messages.
   */
  static void Output(ostream& str);
		
		/** 
			* Sets colorized output.
			* @param colorized Add ANSI colors to output.
			*/
		static void Color(bool colorized);

		/**
			* Create new Error-level message.
   * @param m Message text.
   */
  static void Error(const string & m);
		
		/**
			* Create new Error-level message.
   * @param e Exception
   */
  static void Error(Exception * e);
		
		/**
			* Create new Warning-level message.
   * @param m Message text.
   */
  static void Warning(const string & m);
		
		/**
			* Create new Info-level message.
			* @param m Message text.
			*/
  static void Info(const string & m);
  
		/**
			* Creates new Debug-level message.
			* @param m Message text.
			*/
		static void Debug(const string & m);
  
		/**
			* Retrieve last message reported to log.
			* @return Last reported message.
			*/
		static string last();
};


#endif	/* LOG_H */

