

#include "outputdispatcher.h"

OutputDispatcher::OutputDispatcher(const string& f) : filename(f), line(1), column(1) {
  output.open(filename.c_str(), ios::out);
  if (!output.good())
    throw FileDispatcherException("Could not open out file");
}

OutputDispatcher::~OutputDispatcher() {
  output.close();
}

void OutputDispatcher::write(Element* e) {
  string outputToken;
  outputToken = Formatter::Format(e);
  writeToFile(outputToken);
}

void OutputDispatcher::writeToFile(const string& str) {
  Log::Debug(string("Writing to output: \n  ") += str);
		output << str << flush;
  for (unsigned int i = 0; i < str.size(); ++i) {
				column++;
    if (str[i] == '\n') {
      line++;
						column = 1;
				}
		}
}

int OutputDispatcher::getLineNo() {
  return line;
}

int OutputDispatcher::getColumnNo() {
		return column;
}

string OutputDispatcher::getFileName () {
		return filename;
}
