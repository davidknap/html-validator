#ifndef INPUTDISPATCHER_H
#define	INPUTDISPATCHER_H

#include <string>
#include <iostream>
#include <fstream>
#include <algorithm>
#include "log.h"
#include "../model/token.h"
#include "../exceptions/filedispatcherexception.h"
using namespace std;

/**
	* Input file handler and tokenizer.
	*/
class InputDispatcher {
		/** File handler for input file */
		ifstream input;
		/** Switches parse mode between tag and text */
		bool lastTokenWasTag;
		/** Current line number */
		int line;
public:
		/**
			* Open file for validation.
			* @param filename File name.
			* @throws FileDispatcherException File does not exist or is not readable.
   */
  InputDispatcher(const string& filename);
		
		/**
			* Destructor.
   */
		~InputDispatcher();
		
		/**
			* Get next token (element or block of text) from file.
   * @return Pointer to token.
			* @throws FileDispatcherException Error occured while reading file.
   */
  Token * getToken();
		
		/**
			* Retrieve current line number.
			* @return Input file cursor line number.
			*/
		int getLineNo();
};

#endif	/* INPUTDISPATCHER_H */

