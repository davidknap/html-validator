
#include "inputdispatcher.h"

InputDispatcher::InputDispatcher(const string& filename)
: lastTokenWasTag(true), line(1) {
  input.open(filename.c_str(), ios::in);
  if (!input.good())
    throw FileDispatcherException("Could not open in file");
}

InputDispatcher::~InputDispatcher() {
  input.close();
}

Token* InputDispatcher::getToken() {
  string row;

  while (row.find_first_not_of(" \t\n\f\r") == string::npos && input.good()) {
    if (!lastTokenWasTag)
      getline(input, row, '<'); // input is "some text here ("
    else
      getline(input, row, '>'); // input is "tag here)"
    lastTokenWasTag = !lastTokenWasTag;
				for (unsigned int i = 0; i < row.size(); ++i)
						if (row[i] == '\n')
								line ++;
    
				if ((row.substr(0, 3) == "!--" || row.substr(0, 4) == "<!--") && row.length() >= 5 && row.substr(row.length() - 2) != "--") {
						while (row.substr(row.length() - 3) != "-->" && input.good()) {
								Log::Debug(string("Loading comment, currently: ") += row);
								char c;
								input.get(c);
								row += c;
						}
						row.erase(row.size() - 1);
				}
  }

  unsigned int p = row.find_first_not_of(" \t\n\f\r");
  row.erase(0, p);
  p = row.find_last_not_of(" \t\n\f\r");
  if (string::npos != p)
    row.erase(p + 1);

  p = row.find_first_not_of('<');
  row.erase(0, p);


  if (input.bad()) {
    Log::Error("Could not get next token");
    throw FileDispatcherException("Could not get next token");
  }

  if (input.eof() && row.empty()) {
    Log::Info("Reached end of file.");
    return NULL;
  }

  Token * t = new Token(row, lastTokenWasTag, line);

  return t;
}

int InputDispatcher::getLineNo() {
  return line;
}
