#ifndef FORMATTER_H
#define	FORMATTER_H
#include <string>
#include <sstream>
#include <fstream>
#include <iostream>
#include <deque>
#include "../model/element.h"
#include "../model/tag.h"
#include "../exceptions/logicexception.h"
#include "../exceptions/validityexception.h"
#include "../service/log.h"
#include "../service/outputdispatcher.h"
using namespace std;


class OutputDispatcher;

/**
	* Formatting settings for decorating output
	*/
class Formatter {
		static int indent;
		static char indentChar;
		static unsigned int maxRowLength;
		static deque<string> stack;
		static int currentIndent;
		static char lastChar;
public:
		
		/**
			* Load settings from file.
			* File format is key=value, #-starting and empty lines are ignored.
   */
		static void LoadSettings();
		
		/**
			* Calls element tokenization and formats it.
   * @param e Element to prepare for output
   * @return Output string
   */
		static string Format(Element * e);
		
		/**
			* Returns indentation for use in tag self-formating
   * @return Current indent in formatted string
   */
		static string Indent();
		
		/**
			* Returns indentation advanced by one for use in tag self-formating
   * @return Current indent in formatted string
   */
		static string IndentMore();
		
		/**
			* Go through whole output file and try to keep line length under limit. THIS WILL DESTROY OUTPUT DISPATCHER.
   * @param out Output dispatcher TO BE DESTROYED.
   * @return Always 0. (TODO: Number of lines that were NOT splitted correctly.)
			* @todo Count overfull lines.
   */
		static int WordWrap(OutputDispatcher * out);
		
		/**
			* Return last char in output
   * @return Last char outputted.
   */
		static char LastChar();
		
		/**
			* Static destructor.
			*/
		static void Destroy();
		
};


#endif	/* FORMATTER_H */

