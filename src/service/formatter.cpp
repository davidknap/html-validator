
#include "formatter.h"


int Formatter::indent = 2;
char Formatter::indentChar = ' ';
deque<string> Formatter::stack;
unsigned int Formatter::maxRowLength = 0;
int Formatter::currentIndent = 0;
char Formatter::lastChar = '\n';

void Formatter::LoadSettings() {
		const string configfile = "default.conf";
		
		Log::Debug("Loading configuration...");
		
		ifstream file;
		file.open(configfile.c_str());
		
		if (!file.good()) {
				Log::Warning(string("Could not open configuration file ") + configfile + ", keeping internal default setting.");
				return;
		}
		
		string line;
		while (getline(file, line) && file.good()) {
				if (line.empty() || line[0] == '#')
						continue;
				
				unsigned int sep = line.find('=');
				if (sep >= line.npos) {
						Log::Warning(string("Ignoring invalid line in configuration: ") + line);
						continue;
				}
				string key = line.substr(0, sep);
				string val = line.substr(sep + 1);
				
				if (key == "INDENT_COUNT") {
						indent = atoi(val.c_str());
						Log::Debug(string("Indent count set from configuration file to ") + val);
				}
				else if (key == "INDENT_CHAR") {
						if (val == "SPACE")
								indentChar = ' ';
						else if (val == "TAB")
								indentChar = '\t';
						else if (val.size() == 1)
								indentChar = val[0];
						else {
								Log::Warning("Invalid indent char in configuration file.");
								continue;
						}
						Log::Debug(string("Indent char set from configuration file to ") + val);
				}
				else if (key == "MAX_LINE_LENGTH") {
						if (atoi(val.c_str()) == 0 || atoi(val.c_str()) >= 20) {
								maxRowLength = atoi(val.c_str());
								Log::Debug(string("Max line length set from configuration file to ") + val);
						}
						else
								Log::Warning(string("Invalid max line length: ") + val + ", ignoring");
				}
				else {
						Log::Warning(string("Unknown configuration key ") + val);
				}
		}

		
		file.close();
		Log::Info("Configuration loaded.");
}



string Formatter::Format(Element * e) {
  Log::Debug("Entered formatter");

  string out;

  if (e->plainText) {
    Log::Debug("Formatting as plain text...");
    out = e->name; // TODO line breaks
    if (lastChar == '\n') {
      unsigned int p = out.find_first_not_of("\n");
      out.erase(0, p);
    }
				if (lastChar != '>' && lastChar != ' ')
						out = IndentMore() + out;
		} else {
				switch (e->closeType) {
						case Element::CLOSE_TYPE_OPENING:
								Log::Debug("Formatting as opening tag...");
								stack.push_back(e->name);
								out.append(e->tag->tokenize(e));
								break;
						case Element::CLOSE_TYPE_CLOSING:
								Log::Debug("Formatting as closing tag...");
								if (stack.empty())
										throw ValidityException(string("Attempted to close tag ") += e->name + " but everything is closed.");
								if (e->name != stack.back())
										throw ValidityException(string("Crossing tags, closing ") + e->name + " but " + stack.back() + " not closed.");
								out.append(e->tag->tokenize(e));
								stack.pop_back();
								break;
						case Element::CLOSE_TYPE_SELFCLOSE:
								Log::Debug("Formatting as selfclosing tag...");
								out.append(e->tag->tokenize(e));
								break;
						default:
								throw LogicException(string("Formatter received element with unknown close type: ") += e->closeType);
				}
		}
		
		if (lastChar != ' ' && lastChar != '\t' && lastChar != '\n' && lastChar != '>' && e->closeType == e->CLOSE_TYPE_OPENING && out[0] != indentChar)
				out = string(" ") + out;
  lastChar = *out.rbegin();
  return out;
}

string Formatter::Indent() {
  string out;
  for (unsigned int i = 0; i < stack.size(); ++i)
    for (int j = 0; j < indent; ++j)
      out += indentChar;
  return out;
}

string Formatter::IndentMore() {
  string out;
  for (unsigned int i = 0; i < stack.size() + 1; ++i)
    for (int j = 0; j < indent; ++j)
      out += indentChar;
  return out;
}

int Formatter::WordWrap(OutputDispatcher* out) {
		if (maxRowLength <= 0) {
				delete out;
				return 0;
		}
		
		Log::Info("Post-processing...");
		
		
		try {
				
				string filename = out->getFileName();
				delete out;
				ifstream file;
				file.open(filename.c_str());
				string tempfilename = filename + ".temp";
				ofstream tempfile;
				tempfile.open(tempfilename.c_str());
				string line;
				
				if (!file.good() || !tempfile.good())
						throw FileDispatcherException("Could not open temporary files.");
				
				int lineno = 0;
				while (getline(file, line) && file.good()) {
						lineno ++;
						if (line.length() > maxRowLength) {
								unsigned int s;
								for (unsigned int i = maxRowLength; i < line.length(); i += maxRowLength) {
										Log::Debug(string("Splitting line: ") + line);
										s = line.find_last_of(' ', i);
										/*if (s > i*maxRowLength + prev) {
												stringstream ss;
												ss << "Overfull line " << lineno;
												Log::Warning(ss.str());
										}*/
										line.insert(s, "\n");
								}
						}
						tempfile << line << endl;
				}
				
				tempfile.close();
				file.close();
				
				if (remove(filename.c_str()) != 0)
						throw FileDispatcherException("Could not remove temporary file 1.");
				if (rename(tempfilename.c_str(), filename.c_str()) != 0)
						throw FileDispatcherException("Could not remove temporary file 2.");
				
				Log::Info("Post-processing finished successfully.");
				
		} catch (FileDispatcherException e) {
				Log::Error("Error occured during post-processing:");
				Log::Error(&e);
				Log::Error("Output will be probably corrupted.");
		}
		return 0;
}







char Formatter::LastChar() {
  return lastChar;
}

void Formatter::Destroy() {
		if (!stack.empty())
				throw ValidityException(string("Reached end of file but not all tags closed. Next to close is: ") += stack.back());
		stack.clear();
}

