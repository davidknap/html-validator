
#include "token.h"


Token::Token(const string& str, bool isPlainText, int l) : token(str), plainText(isPlainText), line(l), element(NULL) { }

Token::~Token() {
		if (element) {
				element->attributes.clear();
				delete element;
		}
}

string Token::getString() const {
		return token;
}

bool Token::isPlainText() const {
		return plainText;
}




bool Token::validate(TagLibrary* tags) {
		element = tags->validate(this);
		if (element) {
				return true;
		}	else {
				element = new Element();
				element->name = token;
				return false;
		}
}

void Token::write(OutputDispatcher* output) {
		if (element != NULL)
				output->write(element);
		else
				throw LogicException("Trying to write element that does not exist!");
}
