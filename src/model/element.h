#ifndef ELEMENT_H
#define	ELEMENT_H

#include <string>
#include <list>
using namespace std;

class Tag;

/**
	* Parsed token representation.
	*/
struct Element {
		/** Name or non-structured part of element. */
		string name;
		/** Key-value list of attributes. */
		list<pair <string, string> > attributes;
		/** Type of closing tag. Possible values: Element::CLOSE_TYPE_OPENING, Element::CLOSE_TYPE_CLOSING, Element::CLOSE_TYPE_SELFCLOSE. */
		int closeType;
		/** Pointer to library tag of which type this element is. */
		Tag * tag;
		/** Flag determining if element should pass validity tests. */
		bool plainText;
		
		/** Tag is opening, so stack should be advanced */
		static const int CLOSE_TYPE_OPENING = 0;
		/** Tag is closing, so stack should be substracted */
		static const int CLOSE_TYPE_CLOSING = 1;
		/** Tag is self-closing, so stack should not be changed */
		static const int CLOSE_TYPE_SELFCLOSE = 2;
};



#endif	/* ELEMENT_H */

