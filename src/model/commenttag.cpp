
#include "commenttag.h"

CommentTag::CommentTag(const string & n) : Tag(n) {
 }


bool CommentTag::validate(const string& attribute) const {
		return true;
}


string CommentTag::tokenize(Element* e) const {
		string out = Formatter::IndentMore() + "<";
		if (e->attributes.size())
				out.append(e->attributes.begin()->first);
		out += ">\n";
		return out;
}
