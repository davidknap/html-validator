#ifndef TOKEN_H
#define	TOKEN_H
#include <set>
#include <iostream>
#include <string>

#include "taglibrary.h"
#include "element.h"
#include "../service/outputdispatcher.h"
using namespace std;

class TagLibrary;

/**
	* Single unstructured item picked from input file
	*/
class Token {
		/** Token string */
		string token;
		/** Is token unstructured */
		bool plainText;
		/** Line number in input */
		int line;
		/** Pointer to structured token */
		Element * element;

public:
		/**
			* Constructor of token
   * @param str String of token content
			* @param isPlainText Should token be parsed for internal structure?
			* @param line Line in input file.
   */
		Token(const string & str, bool isPlainText, int line);
		
		/**
			* Destructor
   */
		~Token();
		
		/**
			* Get string representation of token.
   * @return Token string.
   */
		string getString() const;
		
		/**
			* Is token plain text?
   * @return True if it's plain text, false if it's a tag.
   */
		bool isPlainText() const;
		
		/**
			* Validates token against chosen tag library
   * @param tags Tag library
   * @return True if whole token is valid, false if it is not
   */
  bool validate(TagLibrary* tags);
		
		/**
			* Writes token to output
   * @param output Output Dispatcher
   */
  void write(OutputDispatcher* output);
};


#endif	/* TOKEN_H */

