
#include "tag.h"

set<string> Tag::commonAttributes;

Tag::Tag(const string& n) : name(n) { }

Tag::~Tag() {
		attributes.clear();
}


void Tag::addCommonAttribute(const string& attribute) {
		commonAttributes.insert(attribute);
}

void Tag::destroyCommonAttributes() {
		commonAttributes.clear();
}


Tag * Tag::attr(const string& attr) {
		attributes.insert(attr);
		return this;
}

bool Tag::validate(const string& attribute) const {
		set<string>::const_iterator cait = commonAttributes.find(attribute);
		if (cait == commonAttributes.end()) {
				set<string>::const_iterator ait = attributes.find(attribute);
				if (ait == attributes.end())
						return false;
		}
		return true;		
}

