#ifndef TAG_H
#define	TAG_H
#include <set>
#include <string>
#include "../service/formatter.h"
#include "element.h"
using namespace std;

/**
	* Abstract tag representation.
	*/
class Tag {
protected:
		/** Name (or first character set) of tag */
  string name;
		/** List of allowed attributes */
  set<string> attributes;
		/** List of generic attributes allowed everywhere */
		static set<string> commonAttributes;
public:

		/**
			* Constructs new tag.
   * @param name Name of tag.
   */
  Tag(const string& name);
		
		/**
			* Virtual destructor.
   */
		virtual ~Tag();

		/**
			* Add new attribute for tag.
   * @param attr Attribute to add.
   * @return Pointer to self for chaining.
   */
  Tag * attr(const string& attr);
		
		/**
			* Validates single attribute of tag
   * @param attribute Attribute to validate.
   * @return True if attribute is valid, false if not.
   */
  virtual bool validate(const string& attribute) const;
		
		/**
			* Prepares a string of given tag type
   * @param e Filled element structure
   * @return Output token
   */
		virtual string tokenize(Element * e) const = 0;
		
		/**
			* Add new attribute allowed for all tags globally.
   * @param attribute Name of attribute.
   */
		static void addCommonAttribute(const string& attribute);
		
		/**
			* Static destructor for global attribute list.
   */
		static void destroyCommonAttributes();
		
		friend class TagLibrary;
};



#endif	/* TAG_H */

