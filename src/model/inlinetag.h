#ifndef INLINETAG_H
#define	INLINETAG_H

#include "tag.h"

/**
	* Inline-styled tag
 */
class InlineTag : public Tag {
		
		public:
		/**
			* Create new inline tag of given name
			* @param name Tag name.
   */
  InlineTag(const string& name);


		/**
			* Prepare output token for inline-styled tag.
   * @param e Filled element structure.
   * @return String to output.
   */
  virtual string tokenize(Element* e) const;

};


#endif	/* INLINETAG_H */

