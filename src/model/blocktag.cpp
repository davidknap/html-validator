#include "blocktag.h"


BlockTag::BlockTag(const string& n)
: Tag(n) { }

string BlockTag::tokenize(Element* e) const {
  string out;
		
		if (Formatter::LastChar() != '\n')
				out.append("\n");
		if (e->closeType == e->CLOSE_TYPE_SELFCLOSE)
				out.append(Formatter::IndentMore());
		else
				out.append(Formatter::Indent());
  
		out.append("<");
  if (e->closeType == e->CLOSE_TYPE_CLOSING)
    out.append("/");
  out.append(e->name);
  
		for (list<pair <string, string> >::const_iterator it = e->attributes.begin(); it != e->attributes.end(); ++it) {
    out.append(string(" ") + it->first + "=\"" + it->second + "\"");
		}
  
		if (e->closeType == e->CLOSE_TYPE_SELFCLOSE)
    out.append(" /");
  out.append(">");
		if (e->closeType != e->CLOSE_TYPE_SELFCLOSE)
				out.append("\n");
  return out;
}

