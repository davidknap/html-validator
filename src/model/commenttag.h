#ifndef COMMENTTAG_H
#define	COMMENTTAG_H

#include "tag.h"

/**
 * Comment tag.
 */
class CommentTag : public Tag {
public:

  /**
   * Register new comment tag
   * @param n Start sequence of tag
   */
  CommentTag(const string& n);

  /**
   * Tag attribute validation, comments do not have attributes so return true.
   * @param attribute Pseudoargument.
   * @return Always return true.
   */
  virtual bool validate(const string& attribute) const;

  /**
   * Tokenize comment for output.
   * @param e Filled element structure.
   * @return Output string.
   */
  virtual string tokenize(Element* e) const;


};


#endif	/* COMMENTTAG_H */

