#ifndef BLOCKTAG_H
#define	BLOCKTAG_H

#include "tag.h"
#include "../service/formatter.h"

/**
	* Block-styled tag.
 */
class BlockTag : public Tag {
		public:
		
		/**
			* Create new block tag of given name
			* @param name Tag name.
   */
  BlockTag(const string& name);


		/**
			* Prepare output token for block-styled tag.
   * @param e Filled element structure.
   * @return String to output.
   */
  virtual string tokenize(Element* e) const;

};



#endif	/* BLOCKTAG_H */

