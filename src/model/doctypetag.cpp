
#include "doctypetag.h"

DoctypeTag::DoctypeTag(const string& n)
: Tag(n) { }


string DoctypeTag::tokenize(Element * e) const {
		string out = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">";
		return out;
}

bool DoctypeTag::validate(const string& attribute) const {

		return true; 
}
