#ifndef DOCTYPETAG_H
#define	DOCTYPETAG_H

#include "tag.h"

/**
 * Document type definition tag.
 */
class DoctypeTag : public Tag {
public:

		/**
			* Register new Doctype tag
   * @param n Name of tag
   */
  DoctypeTag(const string& n);
		
		
  /**
   * Tokenize doctype for output.
   * @param e Filled element structure.
   * @return Output string.
   */
  virtual string tokenize(Element* e) const;
		

		/**
			* Validate attribute. As no attribute supported, always return true.
   * @param attribute Pseudoargument.
   * @return Always true.
   */
  virtual bool validate(const string& attribute) const;




};



#endif	/* DOCTYPETAG_H */

