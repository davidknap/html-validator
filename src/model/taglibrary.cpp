
#include "taglibrary.h"

TagLibrary::TagLibrary() { }

TagLibrary::~TagLibrary() {
  for (set<Tag*>::iterator it = tags.begin(); it != tags.end(); ++it)
    delete *it;
  tags.clear();
}

set<Tag*>::iterator TagLibrary::add(Tag * t) {
  pair < set<Tag*>::iterator, bool> ret = tags.insert(t);

  if (ret.second == false)
    throw LogicException("Adding tag that already exist");

  return ret.first;
}

Element* TagLibrary::validate(Token* t) {
  Log::Debug(string("Validating token: ") += t->getString());
  Element * e = parseToken(t);
  Log::Debug(string("Token parsed as ") += e->name);

  if (e->plainText)
    return e;

  Tag * tag = findTag(e->name);

  if (tag) {
    e->tag = tag;
    for (list< pair<string, string> >::iterator it = e->attributes.begin(); it != e->attributes.end(); it++) {
      if (!tag->validate(it->first)) {
								string a = it->first;
								delete e;
								throw ValidityException(string("Unknown attribute: ") += a);
						}
    }
  } else {
				string n = e->name;
				delete e;
    throw ValidityException(string("Unknown tag: ") += n);
  }
  return e;
}

Tag* TagLibrary::findTag(const string& name) {
  for (set<Tag*>::iterator it = tags.begin(); it != tags.end(); ++it)
    if ((*it)->name == name)
      return *it;
  return NULL;
}

Element* TagLibrary::parseToken(Token* t) {

  if (t->isPlainText()) {
    Element * e = new Element();
    e->name = t->getString();
    e->plainText = true;
				if (e->name.find('>') != e->name.npos)
						Log::Warning("Closing symbol detected in plain text, you should always use HTML entity instead.");
    return e;
  }

  stringstream ss;
  ss << t->getString();
  Element * e = new Element();
  char c;
  string name = "";
		string attr, val;

  // <a   href  = "123"  />
  //  12  3   4 5 67  89 0

  Log::Debug("Parsing tag name...");
  
		
		// close mark & name
  c = ss.get();
  if (c == '/') {
    e->closeType = e->CLOSE_TYPE_CLOSING;
    c = ss.get();
  }
  while (c != ' ' && c != EOF) {
    name += c; // (1)
    c = ss.get();
  }
  Log::Debug(string("Parsed name: ") += name);

  if (!name.empty() && name[0] == '!') {
    if (name.size() >= 3 && name[1] == '-' && name[2] == '-')
      e->name = "!--";
    if (name.size() >= 8 && (name.substr(1, 7) == "doctype" || name.substr(1, 7) == "DOCTYPE"))
      e->name = "!doctype";

    e->attributes.push_back(make_pair(ss.str(), "dummy"));
    e->closeType = e->CLOSE_TYPE_SELFCLOSE;

    Log::Debug("This is <! starting tag, treating like atomic");

    return e;
  }

  unsigned int trim = name.find_last_not_of(" \t\n\f\r");
  if (string::npos != trim)
    name.erase(trim + 1);
		
		if (name.substr(name.size() - 1) == "/") {
				name.erase(name.size() - 1);
				e->closeType = e->CLOSE_TYPE_SELFCLOSE;
		}
		

  e->name = name;

  Log::Debug(string("Parsing tag attributes..."));
  
		
		// key=value list
  c = ss.get();
  while (c != '/' && c != EOF) {
    while (c == ' ' && c != EOF) // (2), (9)
      c = ss.get();
    while (c != '=' && c != EOF) { // (3)-(5)
      attr += c;
      c = ss.get();
      if (c == '/')
								break;
    }
    if (c == '/')
      break;
    c = ss.get(); // (5)
    while (c == ' ' && c != EOF)
      c = ss.get();
    if (c == '\"' && c != EOF) { // (6)
      c = ss.get();
      while (c != '\"' && c != EOF) { // (7)-(8)
								val += c;
								c = ss.get();
      }
    }
    if (attr == "/")
      break;

    unsigned int p = attr.find_first_not_of(" \t\n\f\r");
    attr.erase(0, p);
    p = attr.find_last_not_of(" \t\n\f\r");
    if (string::npos != p)
      attr.erase(p + 1);

    p = val.find_first_not_of(" \t\n\f\r");
    val.erase(0, p);
    p = val.find_last_not_of(" \t\n\f\r");
    if (string::npos != p)
      val.erase(p + 1);

    Log::Debug(string("Parsed attribute: ") += attr);
    Log::Debug(string("Parsed value: ") += val);
    e->attributes.push_back(make_pair(attr, val));
    attr = "";
    val = "";
    c = ss.get();
  }

  if (c == '/' || attr == "/") {
    Log::Debug("Detected self-closing tag");
    if (e->closeType == e->CLOSE_TYPE_CLOSING)
      throw ValidityException("Tag cannot be closing and self-closing at the same time");
    else
      e->closeType = e->CLOSE_TYPE_SELFCLOSE;
  }

  return e;
}









