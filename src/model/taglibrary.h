#ifndef TAGLIBRARY_H
#define	TAGLIBRARY_H
#include <set>
#include <iostream>
#include <sstream>
#include "tag.h"
#include "token.h"
#include "element.h"
#include "../exceptions/validityexception.h"
#include "../service/log.h"
using namespace std;

class Token;

/**
	* Tools for creating new library of standardized tags.
	*/
class TagLibrary {
protected:
		/** List of allowed tags */
  set<Tag*> tags;


  /**
   * Find corresponding tag for token given.
   * @param name Tag to find.
   * @return Pointer to tag if exist, NULL if does not.
   */
  Tag * findTag(const string& name);
		
		
		/**
			* Parse token to Element structure.
   * @param t Token to parse
   * @return Filled Element structure.
   */
  Element * parseToken(Token * t);
public:
  /**
   * Creates new tag library.
   */
  TagLibrary();
		
		
		/**
			* Virtual destructor.
   */
  virtual ~TagLibrary();


  /**
   * Add new tag to library.
   * @param t Pointer to tag.
   * @return Iterator to added tag.
   * @throws LogicException Tag already existed in library.
   */
  set<Tag*>::iterator add(Tag * t);

  /**
   * Decide if token given is valid tag in this library.
   * Parse token and try to find corresponding tag in library. If found, try
   * to validate all attributes of tag.
   * @param t Token to find tag for.
   * @return Filled element structure or NULL if not valid
   */
  Element * validate(Token * t);
};


#endif	/* TAGLIBRARY_H */

