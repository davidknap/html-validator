#include <cstdlib>
#include <sstream>
#include "model/tag.h"
#include "model/token.h"
#include "model/inlinetag.h"
#include "model/taglibrary.h"

#include "taglibraries/xhtml.h"

#include "service/log.h"
#include "service/inputdispatcher.h"
#include "service/outputdispatcher.h"

#include "exceptions/filedispatcherexception.h"
#include "exceptions/validityexception.h"

using namespace std;

/**
 * @mainpage HTML Validator
	* 
	* Application functionality:
	*
	* @li Parses input file and validates tags and attributes against XHTML 1.0 library.
	* @li Input file should at least start and end by root tag and doctype.
	* @li On the output there is same code well-formatted (indentation etc.).
	* @li Invalid tags are stripped from the output.
	* @li Optinally, line breaking can be applied to output.
	* @li Formatting can be customized by configuration file.
	* 
	* Application limits:
	* 
	* @li App behavior is undefined when there are scripts or style blocks as it is treated like plain text.
	* @li App behavior is undefined when there is non-HTML input like plain text file.
	* @li Line braking will not break extremely long strings like URLs.
	* @li Line breaking may overflow limit due to multibyte characters on line.
	* 
	* Design of code validation:
	* 
	* @image html seq-diagram.svg
	* 
 */
int main(int argc, char** argv) {

  int errorCount = 0;

  try {
				
				// initial configuration
    Log::Verbosity(Log::INFO);
    Log::Output(cout);
    Log::Color(true);

    if (argc != 3) {
      stringstream sse;
      sse << "Usage: " << argv[0] << " <input-name> <output-name>";
      Log::Info(sse.str());
      Log::Error("Invalid parameters");
      Log::Warning("Terminating");
      return 0;
    }
    string inName = argv[1];
    string outName = argv[2];

    InputDispatcher * in;
    OutputDispatcher * out;

    Formatter::LoadSettings();

    try {
      Log::Debug("Opening InputDispatcher");
      in = new InputDispatcher(inName);
      Log::Info("Input file opened successfully.");
    } catch (FileDispatcherException e) {
      Log::Error("Opening of input failed.");
      Log::Warning("Terminating");
      return 1;
    }

    try {
      Log::Debug("Opening OutputDispatcher");
      out = new OutputDispatcher(outName);
      Log::Info("Output file opened successfully.");
    } catch (FileDispatcherException e) {
      Log::Error("Opening of output failed.");
      delete in;
      Log::Warning("Terminating");
      return 1;
    }

    Log::Debug("Building tag library");
    TagLibrary * tags = new XHtmlTagLibrary();
    Log::Debug("Tag library built");

    Token * token;

				
				// validation cycle
    Log::Info("Validating...");

    try {

      while ((token = in->getToken())) {
								Log::Debug(string("Loaded token: ") += token->getString());
								if (token->isPlainText())
										Log::Debug("(as a plain text block)");
								else
										Log::Debug("(as a tag)");

								try {
										if (!(token -> validate(tags)))
												Log::Debug("Validation reporting false");
										token->write(out);
								} catch (ValidityException e) {
										Log::Error("Validation failed: ");
										Log::Error(&e);
										stringstream loc;
										loc << " on line " << in->getLineNo() << ".";
										Log::Error(loc.str());
										Log::Warning(string("Skipping token ") += token->getString() + loc.str());
										Log::Warning("This token will not be present in output file.");
										errorCount++;
								}

								delete token;
      }


						// properly clear tag stack
      try {
								Formatter::Destroy();
      } catch (ValidityException e) {
								Log::Error("Format validation failed:");
								Log::Error(&e);
								errorCount++;
      }


    } catch (FileDispatcherException e) {
      Log::Error("File dispatcher exception occured:");
      Log::Error(&e);
      Log::Warning("Terminating");
      delete in;
      delete out;
      delete tags;
      return 1;
    }
				
				
				// end of validation, print summary
    Log::Info("Validation complete.");
    stringstream ssf;
    ssf << " Read: " << in->getLineNo() << " lines, written: " << out->getLineNo() << " lines.";
    Log::Info(ssf.str());
    
				if (errorCount) {
      stringstream sse;
      sse << "Finished with errors, total errors: " << errorCount;
      Log::Warning(sse.str());
    }

				
				// second step with line breaking, closing no more required parts
				// output dispatcher is destroyed within WordWrap
    Formatter::WordWrap(out);
    delete in;
    delete tags;
    Tag::destroyCommonAttributes();

    Log::Info("Job finished.");
    return 0;
  } catch (LogicException e) {
    Log::Error("Logic exception occured during runtime:");
    Log::Error(&e);
    Log::Warning("Terminating");
    return 1;
  }
}

