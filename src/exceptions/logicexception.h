#ifndef LOGICEXCEPTION_H
#define	LOGICEXCEPTION_H

#include "exception.h"

/**
	* Application logic exception.
	* An unexpected call or variable state.
 */
class LogicException : public Exception {
public:
  /**
   * Creates new logic exception.
   * @param msg Error context.
   */
  LogicException(const string& msg);
};



#endif	/* LOGICEXCEPTION_H */

