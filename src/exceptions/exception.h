#ifndef EXCEPTION_H
#define	EXCEPTION_H

#include <string>
#include <iostream>
using namespace std;


/**
	* Abstract base exception.
	*/
class Exception {
protected:
		/** Text representation of error */
		string message;
public:
		/**
			* Creates new exception
   * @param msg Error message.
   */
		Exception(const string& msg);
		
		/**
			* Virtual method for overriding print.
   * @param os Output stream.
   * @return Output stream.
   */
		virtual ostream& print(ostream& os) const;
		
		/**
			* Retrieve message as simple string.
			* @return String representation of error.
			*/
		virtual string getMessage() const;
		
		/**
			* Output operator.
			*/
		friend ostream& operator << (ostream&, const Exception&);
};


#endif	/* EXCEPTION_H */

