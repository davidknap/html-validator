
#include "exception.h"

Exception::Exception(const string& msg) : message(msg) { }

ostream& Exception::print(ostream& os) const {
  os << message;
  return os;
}

string Exception::getMessage() const {
  return message;
}

ostream& operator<<(ostream& os, const Exception& e) {
  return e.print(os);
}