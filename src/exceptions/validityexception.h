#ifndef VALIDITYEXCEPTION_H
#define	VALIDITYEXCEPTION_H

#include "exception.h"

/**
	* Code validation exception.
	* Validator encountered an invalid code and is promoting it through interfaces.
	*/
class ValidityException : public Exception {
public:
		/**
			* Creates new validation exception.
   * @param msg Error context.
   */
		ValidityException(const string& msg);
};


#endif	/* VALIDITYEXCEPTION_H */

