#ifndef FILEDISPATCHEREXCEPTION_H
#define	FILEDISPATCHEREXCEPTION_H

#include "exception.h"

/**
	* File handling exception
 */
class FileDispatcherException : public Exception {
public:
		/**
			* Creates new exception with given message.
			* @param msg Error context.
			*/
		FileDispatcherException(const string& msg);
};


#endif	/* FILEDISPATCHEREXCEPTION_H */

