#ifndef LOGEXCEPTION_H
#define	LOGEXCEPTION_H

#include "exception.h"

/**
	* Logging service exception.
	*/
class LogException : public Exception {
public:
		/**
			* Creates new log exception.
   * @param msg Error context.
   */
		LogException(const string& msg);
};



#endif	/* LOGEXCEPTION_H */

