
#include "html5.h"



Html5TagLibrary::Html5TagLibrary() : TagLibrary() {
		
  Tag::addCommonAttribute("accesskey");
  Tag::addCommonAttribute("class");
  Tag::addCommonAttribute("contenteditable");
  Tag::addCommonAttribute("contextmenu");
  Tag::addCommonAttribute("dir");
  Tag::addCommonAttribute("draggable");
  Tag::addCommonAttribute("dropzone");
  Tag::addCommonAttribute("hidden");
  Tag::addCommonAttribute("id");
  Tag::addCommonAttribute("lang");
  Tag::addCommonAttribute("spellcheck");
  Tag::addCommonAttribute("style");
  Tag::addCommonAttribute("tabindex");
  Tag::addCommonAttribute("title");
  Tag::addCommonAttribute("translate");
		
		add((new DoctypeTag("!doctype"))->attr("html"));
		
		add((new CommentTag("!--")));

  add((new InlineTag("a"))->attr("href")->attr("target")->attr("rel")
	  ->attr("hreflang")->attr("media")->attr("type"));
  add((new InlineTag("abbr")));
  add((new BlockTag("address")));
  add((new BlockTag("area"))->attr("alt")->attr("href")->attr("target")
	  ->attr("rel")->attr("media")->attr("hreflang")->attr("type")->attr("shape")
	  ->attr("coords"));
  add((new BlockTag("article")));
  add((new BlockTag("aside")));
  add((new BlockTag("audio"))->attr("autoplay")->attr("preload")->attr("controls")
	  ->attr("loop")->attr("mediagroup")->attr("muted")->attr("src"));
  add((new InlineTag("b")));
  add((new BlockTag("base"))->attr("href")->attr("target"));
  add((new BlockTag("bdi")));
  add((new BlockTag("bdo")));
  add((new BlockTag("blockquote"))->attr("cite"));
  add((new BlockTag("body"))->attr("onafterprint")->attr("onbeforeprint")
	  ->attr("onbeforeunload")->attr("onblur")->attr("onerror")->attr("onfocus")
	  ->attr("onhashchange")->attr("onload")->attr("onmessage")->attr("onoffline")
	  ->attr("ononline")->attr("onpagehide")->attr("onpageshow")
	  ->attr("onpopstate")->attr("onresize")->attr("onstorage")->attr("onunload"));
  add((new BlockTag("br")));
  add((new InlineTag("button"))->attr("name")->attr("disabled")->attr("form")
	  ->attr("type")->attr("value")->attr("formaction")->attr("autofocus")
	  ->attr("formenctype")->attr("formmethod")->attr("formtarget")
	  ->attr("formnovalidate"));
  add((new BlockTag("canvas"))->attr("height")->attr("width"));
  add((new InlineTag("caption")));
  add((new InlineTag("cite")));
  add((new BlockTag("code")));
  add((new BlockTag("col"))->attr("span"));
  add((new BlockTag("colgroup"))->attr("span"));
  add((new BlockTag("command"))->attr("type")->attr("label")->attr("icon")
	  ->attr("disabled")->attr("radiogroup")->attr("checked"));
  add((new BlockTag("datalist")));
  add((new BlockTag("dd")));
  add((new InlineTag("del"))->attr("cite")->attr("datetime"));
  add((new BlockTag("details"))->attr("open"));
  add((new BlockTag("dfn")));
  add((new BlockTag("div")));
  add((new BlockTag("dl")));
  add((new BlockTag("dt")));
  add((new InlineTag("em")));
  add((new BlockTag("embed"))->attr("src")->attr("type")->attr("height")
	  ->attr("width"));
  add((new BlockTag("fieldset"))->attr("name")->attr("disabled")->attr("form"));
  add((new BlockTag("figcaption")));
  add((new BlockTag("figure")));
  add((new BlockTag("footer")));
  add((new BlockTag("form"))->attr("action")->attr("method")->attr("enctype")
	  ->attr("name")->attr("accept-charset")->attr("novalidate")->attr("target")
	  ->attr("autocomplete"));
  add((new InlineTag("h1")));
  add((new InlineTag("h2")));
  add((new InlineTag("h3")));
  add((new InlineTag("h4")));
  add((new InlineTag("h5")));
  add((new InlineTag("h6")));
  add((new BlockTag("head")));
  add((new BlockTag("header")));
  add((new BlockTag("hgroup")));
  add((new BlockTag("hr")));
  add((new BlockTag("html"))->attr("manifest")->attr("lang"));
  add((new InlineTag("i")));
  add((new BlockTag("iframe"))->attr("src")->attr("srcdoc")->attr("name")
	  ->attr("width")->attr("height")->attr("sandbox")->attr("seamless"));
  add((new InlineTag("img"))->attr("src")->attr("alt")->attr("height")
	  ->attr("width")->attr("usemap")->attr("ismap")->attr("border"));
  add((new InlineTag("input"))->attr("name")->attr("disabled")->attr("form")
	  ->attr("type")
	  ->attr("maxlength")->attr("readonly")->attr("size")->attr("value")
	  ->attr("autocomplete")
	  ->attr("autofocus")->attr("list")->attr("pattern")->attr("required")
	  ->attr("placeholder")
	  ->attr("dirname")->attr("checked")->attr("formaction")->attr("formenctype")
	  ->attr("formmethod")->attr("formtarget")->attr("formnovalidate")
	  ->attr("acceptd")->attr("multiple")->attr("height")->attr("width")
	  ->attr("alt")->attr("min")->attr("max")->attr("step"));
  add((new InlineTag("ins"))->attr("cite")->attr("datetime"));
  add((new InlineTag("kbd")));
  add((new BlockTag("keygen"))->attr("challenge")->attr("keytype")
	  ->attr("autofocus")->attr("name")->attr("disabled")->attr("form"));
  add((new BlockTag("label"))->attr("for")->attr("form"));
  add((new BlockTag("legend")));
  add((new BlockTag("li"))->attr("value"));
  add((new InlineTag("link"))->attr("href")->attr("rel")->attr("hreflang")
	  ->attr("media")->attr("type")->attr("sizes"));
  add((new BlockTag("map"))->attr("name"));
  add((new BlockTag("mark")));
  add((new BlockTag("menu"))->attr("type")->attr("label"));
  add((new BlockTag("meta"))->attr("name")->attr("content")->attr("http-equiv")
	  ->attr("charset"));
  add((new BlockTag("meter"))->attr("value")->attr("min")->attr("low")
	  ->attr("high")->attr("max")->attr("optimum"));
  add((new BlockTag("nav")));
  add((new BlockTag("noscript")));
  add((new BlockTag("object"))->attr("data")->attr("type")->attr("height")
	  ->attr("width")->attr("usemap")->attr("name")->attr("form"));
  add((new BlockTag("ol"))->attr("start")->attr("reversed")->attr("type"));
  add((new BlockTag("optgroup"))->attr("label")->attr("disabled"));
  add((new InlineTag("option"))->attr("disabled")->attr("selected")->attr("label")
	  ->attr("value"));
  add((new BlockTag("output"))->attr("name")->attr("form")->attr("for"));
  add((new BlockTag("p")));
  add((new BlockTag("param"))->attr("name")->attr("value"));
  add((new BlockTag("pre")));
  add((new BlockTag("progress"))->attr("value")->attr("max"));
  add((new BlockTag("q"))->attr("cite"));
  add((new BlockTag("rp")));
  add((new BlockTag("rt")));
  add((new BlockTag("ruby")));
  add((new BlockTag("s")));
  add((new BlockTag("samp")));
  add((new BlockTag("script"))->attr("type")->attr("language")->attr("src")
	  ->attr("defer")->attr("async")->attr("charset"));
  add((new BlockTag("section")));
  add((new BlockTag("select"))->attr("name")->attr("disabled")->attr("form")
	  ->attr("size")->attr("multiple")->attr("autofocus")->attr("required"));
  add((new InlineTag("small")));
  add((new BlockTag("source"))->attr("src")->attr("type")->attr("media"));
  add((new InlineTag("span")));
  add((new InlineTag("strong")));
  add((new BlockTag("style"))->attr("type")->attr("media")->attr("scoped"));
  add((new BlockTag("sub")));
  add((new BlockTag("summary")));
  add((new BlockTag("sup")));
  add((new BlockTag("table"))->attr("border"));
  add((new BlockTag("tbody")));
  add((new BlockTag("td"))->attr("colspan")->attr("rowspan")->attr("headers"));
  add((new BlockTag("textarea"))->attr("name")->attr("disabled")->attr("form")
	  ->attr("readonly")->attr("maxlength")->attr("autofocus")->attr("required")
	  ->attr("placeholder")->attr("dirname")->attr("rows")->attr("wrap")
	  ->attr("cols"));
  add((new BlockTag("tfoot")));
  add((new BlockTag("th"))->attr("scope")->attr("colspan")->attr("rowspan")
	  ->attr("headers"));
  add((new BlockTag("thead")));
  add((new BlockTag("time"))->attr("datetime"));
  add((new InlineTag("title")));
  add((new BlockTag("tr")));
  add((new BlockTag("track"))->attr("kind")->attr("src")->attr("srclang")
	  ->attr("label")->attr("default"));
  add((new InlineTag("u")));
  add((new BlockTag("ul")));
  add((new BlockTag("var")));
  add((new BlockTag("video"))->attr("autoplay")->attr("preload")->attr("controls")
	  ->attr("loop")->attr("poster")->attr("height")->attr("width")
	  ->attr("mediagroup")->attr("muted")->attr("src"));
  add(new BlockTag("wbr"));
}
