#ifndef HTML5_H
#define	HTML5_H

#include "../model/tag.h"
#include "../model/blocktag.h"
#include "../model/inlinetag.h"
#include "../model/doctypetag.h"
#include "../model/commenttag.h"
#include "../model/taglibrary.h"

/**
 * Represents HTML5 tag library.
 * Tags are created as of http://www.w3.org/TR/html-markup/elements.html
 */
class Html5TagLibrary : public TagLibrary {

		
public:
		
		/**
			* Build tags and attributes.
   */
  Html5TagLibrary();

};



#endif	/* HTML5_H */

