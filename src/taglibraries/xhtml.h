#ifndef XHTML_H
#define	XHTML_H

#include "../model/tag.h"
#include "../model/blocktag.h"
#include "../model/inlinetag.h"
#include "../model/doctypetag.h"
#include "../model/commenttag.h"
#include "../model/taglibrary.h"

/**
 * Represents XHTML tag library.
 * Tags are created as of http://www.w3.org/TR/html-markup/elements.html
 */
class XHtmlTagLibrary : public TagLibrary {

		
public:
		
		/**
			* Build tags and attributes.
   */
  XHtmlTagLibrary();

};



#endif	/* XHTML_H */

