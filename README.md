# HTML Validator

## Application funcionality:

* Parses input file and validates tags and attributes against XHTML 1.0 library.
* Input file should at least start and end by root tag and doctype.
* On the output there is same code well-formatted (indentation etc.).
* Invalid tags are stripped from the output.
* Optinally, line breaking can be applied to output.
* Formatting can be customized by configuration file.
 
## Application limits:

* App behavior is undefined when there are scripts or style blocks as it is treated like plain text.
* App behavior is undefined when there is non-HTML input like plain text file.
* Line braking will not break extremely long strings like URLs.
* Line breaking may overflow limit due to multibyte characters on line.

## Design of code validation:

![Code Validation Diagram](src/seq-diagram.svg)

